/**
 * Rather than create a file with LOADS of stories I found it was better to
 * import stories from the respective component folders. Happy to change if
 * it feels better to do so.
 */
import '../src/Button/Button.stories.js';
import '../src/Heading/Heading.stories.js';