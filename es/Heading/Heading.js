import React from 'react';

var Heading = function Heading(_ref) {
  var children = _ref.children;
  return React.createElement(
    'div',
    null,
    React.createElement(
      'h1',
      null,
      children
    )
  );
};

export default Heading;