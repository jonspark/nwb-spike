/**
 * Using export extensions we can re-export components
 * https://github.com/insin/nwb/blob/master/docs/guides/ReactComponents.md#libraries
 */

import _Button from './Button';
export { _Button as Button };
import _Heading from './Heading';
export { _Heading as Heading };