import React from 'react';

var Button = function Button(_ref) {
  var children = _ref.children,
      _onClick = _ref.onClick;
  return React.createElement(
    'div',
    {
      onClick: function onClick() {
        return _onClick();
      },
      style: {
        backgroundColor: '#61dafb',
        color: 'white',
        padding: '.75rem 1.75rem',
        display: 'inline'
      }
    },
    children
  );
};

export default Button;