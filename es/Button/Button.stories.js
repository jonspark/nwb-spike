import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from './Button';

storiesOf('Button', module).add('with text', function () {
  return React.createElement(
    Button,
    null,
    'Hello Button'
  );
}).add('with emoji', function () {
  return React.createElement(
    Button,
    null,
    React.createElement(
      'span',
      { role: 'img', 'aria-label': 'so cool' },
      '\uD83D\uDE00 \uD83D\uDE0E \uD83D\uDC4D \uD83D\uDCAF'
    )
  );
}).add('with click action', function () {
  return React.createElement(
    Button,
    { onClick: action('clicked') },
    'Click me!'
  );
});