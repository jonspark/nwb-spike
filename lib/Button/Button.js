'use strict';

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function Button(_ref) {
  var children = _ref.children,
      _onClick = _ref.onClick;
  return _react2.default.createElement(
    'div',
    {
      onClick: function onClick() {
        return _onClick();
      },
      style: {
        backgroundColor: '#61dafb',
        color: 'white',
        padding: '.75rem 1.75rem',
        display: 'inline'
      }
    },
    children
  );
};

exports.default = Button;
module.exports = exports['default'];