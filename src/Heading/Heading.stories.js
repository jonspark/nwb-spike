import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import Heading from './Heading';

const stories = storiesOf('Heading', module);

// Add the `withKnobs` decorator to add knobs support to your stories.
// You can also configure `withKnobs` as a global decorator.
stories.addDecorator(withKnobs);

stories
  .add('with static content', () => (
    <Heading>Hello. I am a heading.</Heading>
  ))
  .add('with editable content', () => {
    const title = text('Title', 'Change Me!');
    return <Heading>{title}</Heading>;
  });
