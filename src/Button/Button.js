import React from 'react';

const Button = ({ children, onClick }) => (
  <div
    onClick={() => onClick()}
    style={{
      backgroundColor: '#61dafb',
      color: 'white',
      padding: '.75rem 1.75rem',
      display: 'inline',
    }}
  >
    {children}
  </div>
);

export default Button;
