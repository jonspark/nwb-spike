/**
 * Using export extensions we can re-export components
 * https://github.com/insin/nwb/blob/master/docs/guides/ReactComponents.md#libraries
 */

export Button from './Button';
export Heading from './Heading';